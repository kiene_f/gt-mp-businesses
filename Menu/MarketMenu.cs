﻿using Enums;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using MenuManagement;
using ResurrectionRP.Businesses;
using System;
using System.Collections.Generic;

namespace ResurrectionRP.Menus
{
    class MarketMenu
    {
        private Market _market;
        private PlayerHandler _player;

        public MarketMenu(Client client, NPC ped)
        {
            _player = Players.getPlayerByClient(client);
            _market = Market.getMarketByPed(ped);

            if (_market.isOwner(client))
            {
                Menu menu = new Menu("SuperMarket", "SuperMarket", "Emplacements: " + _market.inventory.currentSize() + "/" + _market.inventory.size, 0, 0, Menu.MenuAnchor.MiddleRight);
                menu.BannerColor = new Color(0, 255, 255, 64);
                menu.Callback = marketOwnerMenuManager;
                menu.BackCloseMenu = true;

                MenuItem item = new MenuItem("Ajouter des produits", "", "ID_add");
                item.ExecuteCallback = true;
                menu.Add(item);
                item = new MenuItem("Reprendre des produits", "", "ID_get");
                item.ExecuteCallback = true;
                menu.Add(item);
                item = new MenuItem("Définir les prix", "", "ID_price");
                item.ExecuteCallback = true;
                menu.Add(item);
                item = new MenuItem("Récupérer l'argent ("+_market.cashBox+" $)", "", "ID_money");
                item.ExecuteCallback = true;
                menu.Add(item);

                MenuManager.OpenMenu(client, menu);
            } else
            {
                Menu menu = new Menu("SuperMarket", "SuperMarket", "Emplacements: " + _market.inventory.currentSize() + "/" + _market.inventory.size, 0, 0, Menu.MenuAnchor.MiddleRight);
                menu.BannerColor = new Color(0, 255, 255, 64);
                menu.Callback = marketMenuManager;
                menu.BackCloseMenu = true;
                if (_market.inventory.inventory.Count > 0)
                {
                    foreach (ItemStack inv in _market.inventory.inventory)
                    {
                        List<string> values = new List<string>();
                        for (int i = 1; i <= inv.quantity; i++) values.Add(i.ToString());
                        MenuItem item = new ListItem(inv.item.name + " (" + inv.price + " $)", inv.item.description, "item", values, 0);
                        item.ExecuteCallback = true;
                        menu.Add(item);
                    }
                }


                MenuManager.OpenMenu(client, menu);
            }
        }

        private void marketOwnerMenuManager(Client client, Menu menu, MenuItem menuItem, int itemIndex, bool forced, dynamic data)
        {
            switch (menuItem.Id)
            {
                case "ID_money":
                    _player.money += _market.cashBox;
                    _market.cashBox = 0;
                    client.triggerEvent(Events.MenuManager_CloseMenu);
                    break;
                case "ID_add":
                    menu = new Menu("SuperMarket", "SuperMarket", "Emplacements: " + _market.inventory.currentSize() + "/" + _market.inventory.size, 0, 0, Menu.MenuAnchor.MiddleRight);
                    menu.BannerColor = new Color(0, 255, 255, 64);
                    menu.Callback = marketAddMenuManager;
                    menu.BackCloseMenu = true;
                    foreach (ItemStack inv in _player.inventory.inventory)
                    {
                        List<string> values = new List<string>();
                        for (int i = 1; i <= inv.quantity; i++) values.Add(i.ToString());
                        MenuItem item = new ListItem(inv.item.name, inv.item.description, "item", values, 0);
                        item.ExecuteCallback = true;
                        menu.Add(item);
                    }
                    MenuManager.OpenMenu(client, menu);
                    break;
                case "ID_get":
                    menu = new Menu("SuperMarket", "SuperMarket", "Emplacements: " + _market.inventory.currentSize() + "/" + _market.inventory.size, 0, 0, Menu.MenuAnchor.MiddleRight);
                    menu.BannerColor = new Color(0, 255, 255, 64);
                    menu.Callback = marketGetMenuManager;
                    menu.BackCloseMenu = true;
                    foreach (ItemStack inv in _market.inventory.inventory)
                    {
                        List<string> values = new List<string>();
                        for (int i = 1; i <= inv.quantity; i++) values.Add(i.ToString());
                        MenuItem item = new ListItem(inv.item.name, inv.item.description, "item", values, 0);
                        item.ExecuteCallback = true;
                        menu.Add(item);
                    }
                    MenuManager.OpenMenu(client, menu);
                    break;
                case "ID_price":
                    menu = new Menu("SuperMarket", "SuperMarket", "Emplacements: " + _market.inventory.currentSize() + "/" + _market.inventory.size, 0, 0, Menu.MenuAnchor.MiddleRight);
                    menu.BannerColor = new Color(0, 255, 255, 64);
                    menu.Callback = marketPriceMenuManager;
                    menu.BackCloseMenu = true;
                    foreach (ItemStack inv in _market.inventory.inventory)
                    {
                        List<string> values = new List<string>();
                        for (int i = 10; i <= 500; i+=10) values.Add(i.ToString());
                        MenuItem item = new ListItem(inv.item.name, inv.item.description, "item", values, 0);
                        item.ExecuteCallback = true;
                        menu.Add(item);
                    }
                    MenuManager.OpenMenu(client, menu);
                    break;
            }
        }

        private void marketPriceMenuManager(Client client, Menu menu, MenuItem menuItem, int itemIndex, bool forced, dynamic data)
        {
            ItemStack itemStack = _market.inventory.inventory[itemIndex];

            int price;
            if (int.TryParse(Convert.ToString(data["item"]["Value"]), out price))
                itemStack.price = price;

            client.triggerEvent(Events.MenuManager_CloseMenu);
        }

        private void marketGetMenuManager(Client client, Menu menu, MenuItem menuItem, int itemIndex, bool forced, dynamic data)
        {
            ItemStack itemStack = _market.inventory.inventory[itemIndex];
            int quantity;
            if (int.TryParse(Convert.ToString(data["item"]["Value"]), out quantity) && 
                _player.inventory.add(client, itemStack.item, quantity))
            _market.inventory.delete(itemStack.item, quantity);

            client.triggerEvent(Events.MenuManager_CloseMenu);
        }

        private void marketAddMenuManager(Client client, Menu menu, MenuItem menuItem, int itemIndex, bool forced, dynamic data)
        {
            ItemStack itemStack = _player.inventory.inventory[itemIndex];

            int quantity;
            if (int.TryParse(Convert.ToString(data["item"]["Value"]), out quantity))
            {
                _market.inventory.add(itemStack.item, quantity);
                _player.inventory.delete(itemStack.item, quantity);
            }

            client.triggerEvent(Events.MenuManager_CloseMenu);
        }

        private void marketMenuManager(Client client, Menu menu, MenuItem menuItem, int itemIndex, bool forced, dynamic data)
        {
            ItemStack itemStack = _market.inventory.inventory[itemIndex];

            int quantity;
            if (int.TryParse(Convert.ToString(data["item"]["Value"]), out quantity) && _player.inventory.add(client, itemStack.item, quantity))
            {
                _player.money -= (itemStack.price * quantity);
                _market.cashBox += (itemStack.price * quantity);
                _market.inventory.delete(itemStack.item, quantity);
            }

            client.triggerEvent(Events.MenuManager_CloseMenu);
        }
    }
}
