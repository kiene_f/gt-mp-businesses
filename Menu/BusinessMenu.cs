﻿using GrandTheftMultiplayer.Server.Elements;
using MenuManagement;
using System;
using System.Collections.Generic;

namespace ResurrectionRP.Menus
{
    class BusinessMenu
    {
        private PlayerHandler _player;
        private Businesses.Businesses _business;

        public BusinessMenu(Client client, Businesses.Businesses business)
        {
            _player = Players.getPlayerByClient(client);
            _business = business; 

            if (_business.isOwner(client))
            {
                Menu mainmenu = new Menu("sell", "Vendre", "", 0, 0, Menu.MenuAnchor.MiddleLeft, backCloseMenu: true);

                if (_business.canEmploy == true)
                {
                    List<string> players = new List<string>();
                    foreach (PlayerHandler player in Players.players)
                        if (player.client.socialClubName != _business.owner &&  !_business.employees.Contains(player.client.socialClubName))
                            players.Add(player.client.socialClubName);

                    ListItem employees = new ListItem("Ajouter un employé:", "", "ID_add", players, 0);
                    employees.ExecuteCallback = true;
                    mainmenu.Add(employees);
                }

                if (_business.canEmploy == true && _business.employees.Count > 0)
                {
                    ListItem employees = new ListItem("Renvoyer un employé:", "", "ID_delete", _business.employees, 0);
                    employees.ExecuteCallback = true;
                    mainmenu.Add(employees);
                }

                MenuItem item = new MenuItem("Vendre l'affaire (" + Convert.ToInt32(_business.businessPrice / 2) + " $)", "", "ID_sell");
                item.ExecuteCallback = true;
                mainmenu.Add(item);

                mainmenu.Callback = ((sender, menu, menuitem, itemindex, forced, data) =>
                {
                    if (menuitem.Id == "ID_sell")
                    {
                        _business.owner = null;
                        _player.client.sendNotificationSuccess("Vous venez de vendre votre affaire pour " + Convert.ToInt32(_business.businessPrice / 2) + " $ !");
                    } else if (menuitem.Id == "ID_add")
                    {
                        _business.employees.Add(data["ID_add"]["Value"].ToString());
                    } else if (menuitem.Id == "ID_delete")
                    {
                        _business.employees.Remove(data["ID_delete"]["Value"].ToString());
                    }
                    MenuManager.CloseMenu(sender);
                });
                MenuManager.OpenMenu(client, mainmenu);
            } else if (!_business.haveOwner())
            {
                Menu mainmenu = new Menu("buy", "Acheter", "", 0, 0, Menu.MenuAnchor.MiddleLeft, backCloseMenu: true);

                MenuItem item = new MenuItem("Acheter l'affaire (" + _business.businessPrice + " $)", "", "ID_buy");
                item.ExecuteCallback = true;
                mainmenu.Add(item);

                mainmenu.Callback = ((sender, menu, menuitem, itemindex, forced, data) =>
                {
                    if (menuitem.Id == "ID_buy")
                    {
                        if (_player.money >= _business.businessPrice)
                        {
                            _player.money -= _business.businessPrice;
                            _business.owner = _player.client.socialClubName;
                            _player.client.sendNotificationSuccess("Vous venez d'acheter votre affaire pour " + _business.businessPrice + " $ !");
                        } else
                            _player.client.sendNotificationError("Vous n'avez pas assez d'argent sur vous !");
                    }
                    MenuManager.CloseMenu(sender);
                });
                MenuManager.OpenMenu(client, mainmenu);
            }
        }
    }
}
