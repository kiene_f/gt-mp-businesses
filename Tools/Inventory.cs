﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ResurrectionRP
{
    public class Inventory
    {
        [JsonProperty("inventory")]
        public List<ItemStack> inventory { get; set; }
        public int size { get; set; }

        public int currentSize()
        {
            int currentSize = 0;
            foreach (ItemStack itemStack in inventory)
                currentSize += (itemStack.item.weight * itemStack.quantity);
            return (currentSize);
        }

        public bool isFull()
        {
            if (currentSize() < size)
                return (false);
            return (true);
        }

        public void add(Item item, int quantity, int price = 0)
        {
            if (currentSize() + (item.weight * quantity) > size) return;
            ItemStack itemStack = inventory.FirstOrDefault(x => x.item.id == item.id);
            if (itemStack != null)
                itemStack.quantity += quantity;
            else
                inventory.Add(new ItemStack(item, quantity, price));
        }

        public void delete(Item item, int quantity)
        {
            ItemStack itemStack = inventory.FirstOrDefault(x => x.item.id == item.id);
            if (itemStack != null)
            {
                if (itemStack.quantity > quantity)
                    itemStack.quantity -= quantity;
                else
                    inventory.Remove(itemStack);
            }
        }

        public void clear(int newsize) => new Inventory(newsize);

        public bool add(Client client, Item item, int quantity)
        {
            if (currentSize() + (item.weight * quantity) > size)
            {
                API.shared.sendNotificationToPlayer(client, "Vous n'avez pas assez de place dans l'inventaire pour ajouter "+ quantity +" "+ item.name, 6);
                return false;
            }
            ItemStack itemStack = inventory.FirstOrDefault(x => x.item.id == item.id);
            if (itemStack != null)
                itemStack.quantity += quantity;
            else
                inventory.Add(new ItemStack(item, quantity));
            API.shared.sendNotificationToPlayer(client, "Vous venez d'ajouter " + quantity + " " + item.name +" dans l'inventaire", 25);
            return true;
        }

        public void delete(Client client, Item item, int quantity)
        {
            ItemStack itemStack = inventory.FirstOrDefault(x => x.item.id == item.id);
            if (itemStack != null)
            {
                if (itemStack.quantity > quantity)
                    itemStack.quantity -= quantity;
                else
                    inventory.Remove(itemStack);
                API.shared.sendNotificationToPlayer(client, "Vous venez de supprimer " + quantity + " " + item.name + " de votre inventaire", 25);
            }
        }

        public static Item itemByID(int id)
        {
            foreach (Item item in LoadItem.ItemsList)
            {
                if (item.id == id) 
                    return item;
            }
            return null;
        }

        public bool checkItem(Item item)
        {
            ItemStack itemStack = inventory.FirstOrDefault(x => x.item.id == item.id);
            if (itemStack != null)
            {
                return true;
            }
            return false;
        }

        public int countItem(Item item)
        {
            ItemStack itemStack = inventory.FirstOrDefault(x => x.item.id == item.id);
            return itemStack.quantity;
        }

        public Inventory(int size)
        {
            inventory = new List<ItemStack>();
            this.size = size;
        }

        public static Inventory fromJson(string json) => JsonConvert.DeserializeObject<Inventory>(json);

        public string toJson() {
            return JsonConvert.SerializeObject(this);
        }
    }
}
