﻿using Newtonsoft.Json;

namespace ResurrectionRP
{
    public class ItemStack
    {
        [JsonProperty("item")]
        public Item item { get; set; }
        [JsonProperty("quantity")]
        public int quantity { get; set; }
        [JsonProperty("price")]
        public int price { get; set; }

        public ItemStack(Item item, int quantity, int price = 0)
        {
            this.item = item;
            this.quantity = quantity;
            this.price = price;
        }
    }
}
