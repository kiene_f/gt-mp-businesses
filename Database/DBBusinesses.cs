﻿using GrandTheftMultiplayer.Server.API;
using Newtonsoft.Json;
using ResurrectionRP.Businesses;
using System;
using System.Data;
using System.Threading.Tasks;

namespace ResurrectionRP
{
    class DBBusinesses: Script
    {
        public DBBusinesses()
        {
            API.onResourceStart += (async () =>
            {
                await Database.waitMysqlReady();
                await loadAllBusinesses();
            });

            API.onResourceStop += (async () =>
            {
                await saveAllBusinesses();
            });
        }

        public static async Task loadAllBusinesses()
        {
            Console.WriteLine("--- Start loading all businesses ---");
            try
            {
                DataTable businesses = await Database.getQuery($"SELECT * FROM businesses");
                foreach (DataRow business in businesses.Rows)
                {
                    switch (Convert.ToString(business["type"]))
                    {
                        case "market":
                            Market market = JsonConvert.DeserializeObject<Market>(Convert.ToString(business["business"]));
                            market.id = Convert.ToInt32(business["id"]);
                            market.load();
                            break;
                        default:
                            JsonConvert.DeserializeObject<Businesses.Businesses>(Convert.ToString(business["business"]));
                            break;
                    }
                }
                Console.WriteLine("--- Finish loading all businesses ---");
            }
            catch (Exception ex)
            {
                API.shared.consoleOutput(ex.ToString());
            }
        }

        public static async Task saveAllBusinesses()
        {
            foreach (Businesses.Businesses business in Businesses.Businesses.businesses)
                await save(business);
        }

        public static async Task save(Businesses.Businesses business)
        {
            string JSONbusiness = JsonConvert.SerializeObject(business);
            if (business.id == -1)
                await insert(JSONbusiness, business);
            else
                await update(JSONbusiness, business);
        }

        public static async Task insert(string JSONbusiness, Businesses.Businesses business)
        {
            int id = await Database.insertQuery(string.Format("INSERT INTO businesses (type, business) VALUES ('{0}', '{1}')", business.type, JSONbusiness));
            business.id = id;
        }

        public static async Task update(string JSONbusiness, Businesses.Businesses business)
        {
            await Database.insertQuery(string.Format("UPDATE businesses SET business='{1}', type='{2}' WHERE id='{0}'",
                business.id,
                JSONbusiness,
                business.type
                ));

        }
    }
}
