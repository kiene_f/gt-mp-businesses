﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ResurrectionRP.Businesses
{
    class Market : Businesses
    {
        [JsonIgnore]
        private static List<Market> _markets = new List<Market>();

        public Inventory inventory { get; set; }
        public Location npc { get; set; }

        public Market(){}

        public Market(Location npc, string owner = null) : base(owner, "market")
        {
            if (inventory == null)
                inventory = new Inventory(200);
            this.npc = npc;
            load();
        }

        public void load()
        {
            NPC vendor = new NPC((PedHash)(416176080), "", npc.pos.toVector3(), (int)npc.rot.z, 0);
            vendor.setData("Interaction", "Market");

            blip = API.shared.createBlip(npc.pos.toVector3());
            API.shared.setBlipSprite(blip, 52);
            API.shared.setBlipShortRange(blip, true);
            API.shared.setBlipName(blip, "SuperMarket");
            if (owner == null)
                API.shared.setBlipColor(blip, color);

            _markets.Add(this);
            businesses.Add(this);
        }

        public bool isOwner(Client client) => client.socialClubName == owner;

        public static Market getMarketByPed(NPC ped) => _markets.Find(x => x.npc.pos.toVector3() == ped.position);
    }
}
