﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ResurrectionRP.Businesses
{
    class Businesses
    {
        [JsonIgnore]
        public static List<Businesses> businesses = new List<Businesses>();
        [JsonIgnore]
        protected Blip blip;
        [JsonIgnore]
        protected int color = 35;

        public string type = "business";
        public int id = -1;
        private string _owner = null; // Social Club Name
        public bool canEmploy = false;
        public List<string> employees = new List<string>(); // Employés
        public int cashBox = 0; // Caisse du magasin
        public int businessPrice = 150000;
        public string businnessName = "";

        public Businesses()
        {
        }

        public Businesses(string owner, string type = "business")
        {
            _owner = owner;
            this.type = type;
        }

        public string owner
        {
            set
            {
                if (value != _owner)
                {
                    _owner = value;
                    if (_owner == null && blip != null)
                        API.shared.setBlipColor(blip, color);
                    else if (blip != null)
                        API.shared.setBlipColor(blip, 4);
                }
            }
            get => _owner;
        }

        public bool haveOwner() => owner != null;

        public bool isOwner(Client client) => client.socialClubName == owner;

        public static Businesses getBusinessByPed(NPC ped)
        {
            foreach (Businesses business in businesses)
            {
                if (business is Barber barber && barber.npc.Item2.pos.toVector3() == ped.position)
                    return (business);
                else if (business is Market market && market.npc.pos.toVector3() == ped.position)
                    return (business);
                else if (business is Tatoo tatoo && tatoo.npc.pos.toVector3() == ped.position)
                    return (business);
            }
            return (null);
        }
    }
}
